Splynx Add-on Testla
======================

Splynx Add-on Testla based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-base-2.git
cd splynx-addon-base-2
composer install
~~~

Install Splynx Add-on Testla:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-testla.git
cd splynx-addon-testla
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-testla/web/ /var/www/splynx/web/testla
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-testla.addons
~~~

with following content:
~~~
location /testla
{
        try_files $uri $uri/ /testla/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

You can then access Splynx Add-On Testla in a menu "Customers".