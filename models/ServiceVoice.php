<?php

namespace app\models;

use splynx\models\services\VoiceService;
use splynx\v2\models\services\BaseVoiceService;
use yii\db\ActiveRecord;
use yii\base\Model;

/**
 * Class ServiceVoice
 * @package app\models
 */
class ServiceVoice extends BaseVoiceService
{
    public $description;
    public $unit_price;

    public function rules()
    {
        return [
            [['description'], 'required'],
        ];
    }

    public function getVoiceServicesByCustomerId($customer_id)
    {
        return VoiceService::findByCustomerId($customer_id);
    }

    public function getById($id)
    {
        $voiceService = new BaseVoiceService();
        return $voiceService->findById($id);
    }

    public function getParamsUrl()
    {
        return $_GET['customer_id'];
    }
}