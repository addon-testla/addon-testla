<?php

namespace app\models;

use splynx\v2\models\customer\BaseCustomer;
use app\models\Internet;
use app\models\ServiceVoice;
use app\models\Recurring;
use app\models\Bundle;
use yii\base\Model;

/**
 * Class Customer
 * @package app\models
 */
class Customer extends BaseCustomer
{
    public $login;
    public $name;
    public $password;
    public $email;
    public $street_1;
    public $phone;

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['email', 'email'],
            [['name'], 'string'],
        ];
    }

    /**
     * @return Customer[]|null
     */
    public function getFiveLastAdded()
    {
        return $this->findAll(
            [],
            [],
            [
                'id' => 'DESC'
            ]
        );
    }

    public function getInternetServices($customer_id)
    {
        return Internet::getInternetServicesByCustomerId($customer_id);
    }

    public function getVoiceServices($customer_id)
    {
        return ServiceVoice::getVoiceServicesByCustomerId($customer_id);
    }

    public function getRecurringServices($customer_id)
    {
        return Recurring::getRecurringServicesByCustomerId($customer_id);
    }
}
