<?php

namespace app\models;

use splynx\models\services\InternetService;
use splynx\v2\models\services\BaseInternetService;
use app\models\Yii;

/**
 * Class Internet
 * @package app\models
 */
class Internet extends BaseInternetService
{
    public $description;
    public $unit_price;
    public $password;
    public $login;

    public function rules()
    {
        return [
            [['description', 'password', 'login'], 'required'],
        ];
    }

    public function getInternetServicesByCustomerId($customer_id)
    {
        return InternetService::findByCustomerId($customer_id);
    }

    public function getById($id)
    {
        $internetService = new BaseInternetService();
        return $internetService->findById($id);
    }

    public function getParamsUrl()
    {
        return $_GET['id'];
    }
}