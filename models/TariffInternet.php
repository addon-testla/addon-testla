<?php

namespace app\models;

use splynx\models\services\InternetService;
use splynx\v2\models\services\BaseInternetService;
use splynx\v2\models\tariffs\BaseInternetTariff;

/**
 * Class TariffInternet
 * @package app\models
 */
class TariffInternet extends BaseInternetTariff
{
    public function getInternetServicesByCustomerId($customer_id)
    {
        return InternetService::findByCustomerId($customer_id);
    }

    public function getById($id)
    {
        return $this->findById($id);
    }

    public function getAllInternetTariffsId()
    {
        return TariffInternet::getTariffTitles();
    }

    public function getAllInternetTariffs()
    {
        $condition = ['customer_id' => ''];
        $newInternetTariff = new BaseInternetTariff();
        return $newInternetTariff->find($condition, null);
    }

    public function getTariffTitles()
    {
        $allInternetTariffs = self::getAllInternetTariffs();
        $tariffIds = [];
        foreach ($allInternetTariffs as $tariffById) {
            $tariffIds[$tariffById['id']] = $tariffById['title'];
        }
        return $tariffIds;
    }
}