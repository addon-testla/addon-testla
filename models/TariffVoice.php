<?php

namespace app\models;

use splynx\models\services\InternetService;
use splynx\v2\models\services\BaseVoiceService;
use splynx\v2\models\tariffs\BaseVoiceTariff;

/**
 * Class TariffVoice
 * @package app\models
 */
class TariffVoice extends BaseVoiceTariff
{
    public function getInternetServicesByCustomerId($customer_id)
    {
        return InternetService::findByCustomerId($customer_id);
    }

    public function getById($id)
    {
        return $this->findById($id);
    }

    public function getAllVoiceTariffsId()
    {
        return TariffVoice::getTariffTitles();
    }

    public function getAllVoiceTariffs()
    {
        $condition = ['customer_id' => ''];
        $newInternetTariff = new BaseVoiceTariff();
        return $newInternetTariff->find($condition, null);
    }

    public function getTariffTitles()
    {
        $allInternetTariffs = self::getAllVoiceTariffs();
        $tariffIds = [];
        foreach ($allInternetTariffs as $tariffById) {
            $tariffIds[$tariffById['id']] = $tariffById['title'];
        }
        return $tariffIds;
    }
}