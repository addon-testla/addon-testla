<?php

namespace app\models;

use splynx\v2\models\services\BaseCustomService;

/**
 * Class Recurring
 * @package app\models
 */
class Recurring extends BaseCustomService
{
    public $description;
    public $unit_price;

    public function rules()
    {
        return [
            [['description'], 'required'],
        ];
    }

    public function getRecurringServicesByCustomerId($customer_id)
    {
        $array_customer_id = ['id' => $customer_id];
        $newRecurring = new BaseCustomService();
        return $newRecurring->find($array_customer_id, null);
    }

    public function getById($id)
    {
        $customservice = new BaseCustomService();
        return $customservice->findById($id);
    }

    public function getParamsUrl()
    {
        return $_GET['customer_id'];
    }
}