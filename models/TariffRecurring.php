<?php

namespace app\models;

use splynx\v2\models\services\BaseCustomService;
use splynx\v2\models\tariffs\BaseCustomTariff;

/**
 * Class TariffRecurring
 * @package app\models
 */
class TariffRecurring extends BaseCustomTariff
{
    public function getInternetServicesByCustomerId($customer_id)
    {
        return CustomService::findByCustomerId($customer_id);
    }

    public function getById($id)
    {
        return $this->findById($id);
    }

    public function getAllRecurringTariffsId()
    {
        return TariffRecurring::getTariffTitles();
    }

    public function getAllRecurringTariffs()
    {
        $condition = ['customer_id' => ''];
        $newInternetTariff = new BaseCustomTariff();
        return $newInternetTariff->find($condition, null);
    }

    public function getTariffTitles()
    {
        $allInternetTariffs = self::getAllRecurringTariffs();
        $tariffIds = [];
        foreach ($allInternetTariffs as $tariffById) {
            $tariffIds[$tariffById['id']] = $tariffById['title'];
        }
        return $tariffIds;
    }
}