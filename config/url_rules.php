<?php

return [
    '<alias:index>' => 'site/<alias>',
    '<alias:/view/<id:\d+>>' => 'site/<alias>',
    
    '<alias:/add-internet/<customer_id:\d+>>' => 'site/<alias>',
    '<alias:/add-voice/<customer_id:\d+>>' => 'site/<alias>',
    '<alias:/add-recurring/<customer_id:\d+>>' => 'site/<alias>',

    '<alias:/edit-customer/<id:\d+>>' => 'site/<alias>',
    '<alias:/edit-internet/<id:\d+>>' => 'site/<alias>',
    '<alias:/edit-voice/<id:\d+>>' => 'site/<alias>',
    '<alias:/edit-recurring/<id:\d+>>' => 'site/<alias>',

    '<alias:/delete-internet/<id:\d+>>' => 'site/<alias>',
    '<alias:/delete-voice/<id:\d+>>' => 'site/<alias>',
    '<alias:/delete-recurring/<id:\d+>>' => 'site/<alias>',
];
