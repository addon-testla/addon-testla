<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/testla',
                'enableCookieValidation' => false
            ],
            'user' => [
                'identityClass' => 'splynx\models\Admin',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Ftestla%2F',
                'enableAutoLogin' => false,
            ],
            'assetManager' => [
                'bundles' => [
                    'yidas\yii\fontawesome\FontawesomeAsset' => [
                        'cdn' => true,
                    ],
                ],
            ],
        ],
    ];
};
