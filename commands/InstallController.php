<?php

namespace app\commands;

use splynx\base\BaseInstallController;

/**
 * Class InstallController
 * @package app\commands
 */
class InstallController extends BaseInstallController
{
    /**
     * @inheritdoc
     */
    public $module_status = self::MODULE_STATUS_ENABLED;

    /**
     * @inheritdoc
     */
    public static $minimumSplynxVersion = '3.1';

    /**
     * @inheritdoc
     * @return string
     */
    public function getAddOnTitle()
    {
        // Db column varchar(64)
        return 'Splynx Add-on TestLA';
    }

    /**
     * @inheritdoc
     */
    public function getModuleName()
    {
        // Db column varchar(32)
        return 'splynx_addon_testla';
    }

    /**
     * @inheritdoc
     */
    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index','view','update'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerVoiceServices',
                'actions' => ['index','view','update','add','delete'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerRecurringServices',
                'actions' => ['index','view','update','add','delete'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerInternetServices',
                'actions' => ['index','view','update','add','delete',],
            ],
            [
                'controller' => 'api\admin\customers\CustomServices',
                'actions' => ['index','view','update','add','delete'],
            ],
            [
                'controller' => 'api\admin\tariffs\Custom',
                'actions' => ['index','view','update','add','delete'],
            ],
            [
                'controller' => 'api\admin\tariffs\Internet',
                'actions' => ['index','view','update','add','delete'],
            ],
            [
                'controller' => 'api\admin\tariffs\Recurring',
                'actions' => ['index','view','update','add','delete'],
            ],
            [
                'controller' => 'api\admin\tariffs\Voice',
                'actions' => ['index','view','update','add','delete'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEntryPoints()
    {
        return [
            [
                'name' => 'testla_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Ftestla%2F',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }
}
