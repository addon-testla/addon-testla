<?php

namespace app\controllers;

use app\models\Customer;
use app\models\Internet;
use app\models\Recurring;
use app\models\TariffInternet;
use app\models\TariffVoice;
use app\models\TariffRecurring;
use app\models\ServiceVoice;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\controllers\Yii;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->getView()->title = 'Splynx Add-on TestLA';
        return $this->render(
            'index',
            array('model' => new Customer())
        );
    }

    public function actionView($id)
    {
        $model = Customer::findIdentity($id);

        $this->getView()->title = 'List all services';
        return $this->render(
            'view',
            array('model' => $model)
        );
    }

    public function actionAddInternet($customer_id)
    {
        $tariff_ids = TariffInternet::getAllInternetTariffsId();
        $form = new Internet();
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = \Yii::$app->request;
            $form->description = $request->post('Internet')['description'];
            $form->unit_price = $request->post('Internet')['unit_price'];
            $form->password = $request->post('Internet')['password'];
            $form->login = $request->post('Internet')['login'];
            $form->tariff_id = $request->post('Internet')['tariff_id'];
            $form->customer_id = $customer_id;
            $form->save();
            \Yii::$app->getSession()->setFlash('success', 'Internet Service has been successfully added.');
            $this->getView()->title = 'Add Internet Service';
            return $this->redirect('/testla/site/view?id=' . $form->customer_id);
        } else {
            return $this->render('add-internet', ['model' => $form, 'tariff_ids' => $tariff_ids]);
        }
    }

    public function actionAddVoice($customer_id)
    {
        $tariff_ids = TariffVoice::getAllVoiceTariffsId();
        $form = new ServiceVoice();
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = \Yii::$app->request;
            $form->description = $request->post('ServiceVoice')['description'];
            $form->unit_price = $request->post('ServiceVoice')['unit_price'];
            $form->tariff_id = $request->post('ServiceVoice')['tariff_id'];
            $form->customer_id = $customer_id;
            $form->save();
            \Yii::$app->getSession()->setFlash('success', 'Voice Service has been successfully added.');
            $this->getView()->title = 'Add Voice Service';
            return $this->redirect('/testla/site/view?id=' . $form->customer_id);
        } else {
            return $this->render('add-voice', ['model' => $form, 'tariff_ids' => $tariff_ids]);
        }
    }

    public function actionAddRecurring($customer_id)
    {
        $tariff_ids = TariffRecurring::getAllRecurringTariffsId();
        $form = new Recurring();
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = \Yii::$app->request;
            $form->description = $request->post('Recurring')['description'];
            $form->unit_price = $request->post('Recurring')['unit_price'];
            $form->tariff_id = $request->post('Recurring')['tariff_id'];
            $form->customer_id = $customer_id;
            $form->save();
            \Yii::$app->getSession()->setFlash('success', 'Recurring Service has been successfully added.');
            $this->getView()->title = 'Add Recurring Service';
            return $this->redirect('/testla/site/view?id=' . $form->customer_id);
        } else {
            return $this->render('add-recurring', ['model' => $form, 'tariff_ids' => $tariff_ids]);
        }
    }

    public function actionEditCustomer($id)
    {
        $form = Customer::findIdentity($id);
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = \Yii::$app->request;
            $form->login = $request->post('Customer')['login'];
            $form->name = $request->post('Customer')['name'];
            $form->email = $request->post('Customer')['email'];
            $form->password = $request->post('Customer')['password'];
            $form->street_1 = $request->post('Customer')['street_1'];
            $form->phone = $request->post('Customer')['phone'];
            $form->save();
            \Yii::$app->getSession()->setFlash('success', 'Customer has been successfully updated.');
            $this->getView()->title = 'Edit Customer';
            return $this->redirect('/testla');
        } else {
            return $this->render('edit-customer', ['model' => $form]);
        }
    }

    public function actionEditInternet($id)
    {
        $tariff_ids = TariffInternet::getAllInternetTariffsId();
        $form = Internet::getById($id);
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = \Yii::$app->request;
            $form->description = $request->post('BaseInternetService')['description'];
            $form->unit_price = $request->post('BaseInternetService')['unit_price'];
            $form->password = $request->post('BaseInternetService')['password'];
            $form->login = $request->post('BaseInternetService')['login'];
            $form->tariff_id = $request->post('BaseInternetService')['tariff_id'];
            $form->save();
            \Yii::$app->getSession()->setFlash('success', 'Internet Service has been successfully updated.');
            $this->getView()->title = 'Edit internet Service';
            return $this->redirect('/testla/site/view?id=' . $form->customer_id);
        } else {
            return $this->render('edit-internet', ['model' => $form, 'tariff_ids' => $tariff_ids]);
        }
    }

    public function actionEditVoice($id)
    {
        $tariff_ids = TariffVoice::getAllVoiceTariffsId();
        $form = ServiceVoice::getById($id);
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = \Yii::$app->request;
            $form->description = $request->post('BaseVoiceService')['description'];
            $form->unit_price = $request->post('BaseVoiceService')['unit_price'];
            $form->tariff_id = $request->post('BaseVoiceService')['tariff_id'];
            $form->save();
            \Yii::$app->getSession()->setFlash('success', 'Voice Service has been successfully updated.');
            $this->getView()->title = 'Edit Voice Service';
            return $this->redirect('/testla/site/view?id=' . $form->customer_id);
        } else {
            return $this->render('edit-voice', ['model' => $form, 'tariff_ids' => $tariff_ids]);
        }
    }

    public function actionEditRecurring($id)
    {
        $tariff_ids = TariffRecurring::getAllRecurringTariffsId();
        $form = Recurring::getById($id);
        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            $request = \Yii::$app->request;
            $form->description = $request->post('BaseCustomService')['description'];
            $form->unit_price = $request->post('BaseCustomService')['unit_price'];
            $form->tariff_id = $request->post('BaseCustomService')['tariff_id'];
            $form->save();
            \Yii::$app->getSession()->setFlash('success', 'Recurring Service has been successfully updated.');
            $this->getView()->title = 'Edit Recurring Service';
            return $this->redirect('/testla/site/view?id=' . $form->customer_id);
        } else {
            return $this->render('edit-recurring', ['model' => $form, 'tariff_ids' => $tariff_ids]);
        }
    }

    public function actionDeleteInternet($id)
    {
        $model = Internet::getById($id);
        $model->delete();
        \Yii::$app->getSession()->setFlash('success', 'Internet Service has been successfully deleted.');
        return $this->redirect('/testla/site/view?id=' . $model->customer_id);
    }

    public function actionDeleteVoice($id)
    {
        $model = ServiceVoice::getById($id);
        $model->delete();
        \Yii::$app->getSession()->setFlash('success', 'Voice Service has been successfully deleted.');
        return $this->redirect('/testla/site/view?id=' . $model->customer_id);
    }

    public function actionDeleteRecurring($id)
    {
        $model = Recurring::getById($id);
        $model->delete();
        \Yii::$app->getSession()->setFlash('success', 'Recurring Service has been successfully deleted.');
        return $this->redirect('/testla/site/view?id=' . $model->customer_id);
    }
}
